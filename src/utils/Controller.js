'user strict';

var response = require("./res");
var connection = require("./Conn");

exports.e_hrd = function (req, res) {
    connection.query("SELECT * FROM user_account", function (error, rows) {
        if (error) {
            console.log(error);
        } else {
            response.ok(rows, res);
        }
    });
};

exports.createHrd = function (req, res) {
    var user_id = req.body.user_id;
    var user_username = req.body.user_username;
    var user_password = req.body.user_password;

    connection.query(
        "INSERT INTO user_account (user_id, user_username, user_password) values (?,?,?)",
        [user_id, user_username, user_password],
        function (error, rows, fields) {
            if (error) {
                console.log(error);
            } else {
                response.ok("Berhasil menanmbahkan user account", res);
            }
        }
    )
}

exports.index = function (req, res) {
    response.ok("Hello from the Node Js RESful side", res);
};