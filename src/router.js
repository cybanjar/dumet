import Vue from "vue";
import VueRouter from "vue-router";

import About from "./views/About";
import Home from "./views/Home";
import Login from "./views/Login";
import Register from "./views/Register";
import Qiro from "./views/Qiro";
import Alfateha from "./views/Alfateha";
import Uthmani from "./views/Uthmani";
import DetailSurah from "./views/DetailSurah";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    { path: "/", component: Qiro },
    { path: "/home", component: Home },
    { path: "/login", component: Login },
    { path: "/register", component: Register },
    { path: "/about", component: About },
    { path: "/1", component: Alfateha },
    { path: "/uthmani", component: Uthmani },
    { path: "/detail-surah", component: DetailSurah },
  ],
});

export default router;
